class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(player)
    @guesser = player[:guesser]
    @referee = player[:referee]
  end

  def setup
    @secret_word_length = @referee.pick_secret_word
    @guesser.register_secret_length(@secret_word_length)
    @board = Array.new(@secret_word_length, nil)
  end

  def take_turn
    puts "Please guess a letter"
    guess = @guesser.guess
    indices = @referee.check_guess(guess)
    update_board(guess, indices)
    @guesser.handle_response(guess, indices)
  end

  def update_board(guess, indices)
    updated_board = []
    @board.each_with_index do |current_holder, i|
      if indices.include?(i)
        updated_board << guess
      else
        updated_board << current_holder
      end
    end
    @board = updated_board
  end

end

class HumanPlayer

  def register_secret_length(length)
    puts "the length of the secret word is #{length}"
  end

  def guess
    puts "please guess a letter"
    guess = gets.chomp
  end

  def handle_response(check, guess)
    if check.length >= 1
      puts "#{guess} occurs at the amount of #{check.length}"
    else
      puts "#{guess} is not included in the word"
    end
  end

  def check_guess(letter_guess)
  #this method needs to ask the human player if the letter guess occurs in the word and if so where?
  puts "does #{letter_guess} occur in your word?(true/false)"
  does_occurs = gets.chomp
  puts "if so, then where?" if does_occurs

  gets.chomp.split(',').map {|num| Integer(num)}
  end

end

class ComputerPlayer

  def initialize(dictionary = File.readlines("dictionary.txt"))
    @dictionary = dictionary
  end

  attr_reader :candidate_words

  def pick_secret_word
    @@secret_word = @dictionary.sample.strip
    return @@secret_word.length
  end

  def check_guess(letter_guess)
    location = []
    @@secret_word.each_char.with_index {|letter, idx| location << idx if letter == letter_guess}
    location
  end

  def register_secret_length(length)
    @candidate_words = @dictionary.select { |word| word.length == length }
  end

  def guess(board)
  freq_hash = create_freq_hash(board)
  most_common(freq_hash)
  end

  def most_common(hash)
    occurs_most = ''
    occur_amount = 0
      hash.each do |key, val|
        if val > occur_amount
          occurs_most = key
          occur_amount = val
        end
      end
    occurs_most
  end

  def create_freq_hash(board)
    #this create a hash of the frequency of characters in a list of words
    hash = Hash.new(0)
    @candidate_words.each do |word|
      board.each_with_index do |letter, idx|
        hash[word[idx]] += 1 if letter.nil?
      end
    end
    hash
  end

  def handle_response(guess, found_indices)
    # condsidering the guess and the location of the letter select all remaining words
    # that have the guessed letter at the correct location in each word
    if found_indices.empty?
      # if you search for a letter at no indices thats like asking "search for this letter thats at the position of nowhere"
      # which just means search for words that dont have this letter
      @candidate_words.reject! { |word| word.include?(guess) }
    else
      @candidate_words.select! { |word| word.include?(guess) && contending_word?(word, guess, found_indices) }
    end
  end

  def contending_word?(word, letter_guess, found_indices)
  # this helper function needs to check if the given word includes the letter_guess at the
  # correct index
    word.chars.each_with_index do |letter, idx|
      return false if letter == letter_guess && !found_indices.include?(idx)
      return false if letter != letter_guess && found_indices.include?(idx)
    end
    true
  end
end

if __FILE__ == $PROGRAM_NAME
  print "Guesser: Computer (yes/no)? "
  if gets.chomp == "yes"
    guesser = ComputerPlayer.new
  else
    guesser = HumanPlayer.new

  print "Referee: Computer (yes/no)? "
  if gets.chomp == "yes"
    referee = ComputerPlayer.new
  else
    referee = HumanPlayer.new
  end

  Hangman.new({guesser: guesser, referee: referee}) #.play 

end
